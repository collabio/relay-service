export class RelayService {
    public constructor(io: SocketIO.Server) {

        let lastId = 0;
        let users = {};
        let rooms = {};

        function roomId(d): string {
            return d.room + '|' + d.ns;
        }

        io.of('relay').on('connection', socket => {

            var id = ++lastId;
            var inRooms = [];
            users[id] = socket;

            socket.on('join', room => {
                if (rooms[room.room] == null)
                    rooms[room.room] = { namespaces: { [room.ns]: [] } };
                else if (rooms[room.room].namespaces[room.ns] == null)
                    rooms[room.room].namespaces[room.ns] = [];

                for (let peer of rooms[room.room].namespaces[room.ns])
                    socket.emit('addToRoom', { room, peer });

                socket.broadcast.to(roomId(room)).emit('addToRoom', { room: room, peer: id });

                socket.join(roomId(room));
                rooms[room.room].namespaces[room.ns].push(id);
                inRooms.push(room);

                console.log('#' + id + ' joined');
            });

            socket.on('direct', direct => {
                direct.sender = id;
                if (users[direct.peer])
                    users[direct.peer].emit(direct.event, direct);
                else
                    console.log(`Direct message could not be sent from #${direct.sender} to #${direct.peer}.`);
            });

            socket.on('broadcast', broadcast => {
                socket.broadcast.to(roomId(broadcast)).emit(broadcast.event, broadcast);
            });

            socket.on('disconnect', () => {
                delete users[id];

                inRooms.forEach(room => {
                    rooms[room.room].namespaces[room.ns].splice(rooms[room.room].namespaces[room.ns].indexOf(id), 1);
                    socket.broadcast.to(roomId(room)).emit('removeFromRoom', { room: room, peer: id });
                });

                console.log('#' + id + ' left');
            });
        });

    }
}